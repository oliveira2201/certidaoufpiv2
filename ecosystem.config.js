// a
module.exports = {
  apps: [
    {
      name: 'certidaoUFPI',
      script: './dist/servidor.js',
      instances: 1,
      autorestart: true,
      watch: false,
      max_memory_restart: '1G',
      env_production: {
        NODE_ENV: 'production'
      },
      ignore_watch: ['node_modules', './dist/download'],
      node_args: ['--no-warnings']
    }
  ],

  deploy: {
    production: {
      user: 'ec2-user',
      host: 'ec2-18-223-111-187.us-east-2.compute.amazonaws.com',
      key: 'C:/Users/olive/desktop-rhat.pem',
      ref: 'origin/master',
      repo: 'git@bitbucket.org:oliveira2201/certidaoufpiv2.git',
      path: '/home/ec2-user/certidaoUFPI',
      'post-deploy':
        'npm install && npm run build && pm2 startOrRestart ecosystem.config.js  --env production'
    }
  }
};
