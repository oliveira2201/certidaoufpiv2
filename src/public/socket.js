var conectado = false;
var pronto = true;

function appendNotification(notification, color) {
  var d1 = document.getElementById('log');
  var d2 = '<tr><td>' + notification + '</td></tr>';
  d1.insertAdjacentHTML('beforeend', d2);
}

var socket = io();
socket.on('mensagem', function(msg) {
  appendNotification(msg);
});

socket.on('notificacao', function(msg) {
  console.log(msg);
  if (msg === 'iniciado') pronto = false;
  if (msg === 'terminado') pronto = true;
  if (msg === 'conectado') conectado = true;
  if (msg === 'desconectado') conectado = false;
  HabilitarBotao();
});

socket.on('connect_error', function(err) {
  $('#submeter').prop('disabled', true);
  conectado = false;
  HabilitarBotao();
});
