function mascara(el, masc) {
  el.value = masc(el.value);
  ValidarCNPJ(el)
}
function soNumeros(d) {
  return d.replace(/\D/g, "");
}
function cpf_mask(d) {
  d = soNumeros(d);
  d = d.replace(/(\d{3})(\d)/, "$1.$2");
  d = d.replace(/(\d{3})(\d)/, "$1.$2");
  d = d.replace(/(\d{3})(\d{1,2})$/, "$1-$2");
  return d;
}
function cnpj_mask(d) {
  d = soNumeros(d);
  d = d.replace(/^(\d{2})(\d)/, "$1.$2");
  d = d.replace(/^(\d{2})\.(\d{3})(\d)/, "$1.$2.$3");
  d = d.replace(/\.(\d{3})(\d)/, ".$1/$2");
  d = d.replace(/(\d{4})(\d)/, "$1-$2");
  return d;
}
