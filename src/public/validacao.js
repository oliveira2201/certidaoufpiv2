var valido = false;
var reiniciar = false;

$(document).ready(function() {
  HabilitarBotao();
  $('#submeter').click(function() {
    var data = $('#formulario').serialize();
    var data2 = data + '&id=' + socket.id;
    this.disabled = true;
    $.ajax({
      url: '/',
      type: 'post',
      dataType: 'json',
      data: data2,
      success: function(data) {}
    });
    if (reiniciar) {
      $('#log').html('');
    }
    reiniciar = true;
  });
});

function ValidarCNPJ(ObjCnpj) {
  var cnpj = ObjCnpj.value;
  var valida = new Array(6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2);
  var dig1 = new Number();
  var dig2 = new Number();

  var exp = /\.|\-|\//g;
  cnpj = cnpj.toString().replace(exp, '');
  var digito = new Number(eval(cnpj.charAt(12) + cnpj.charAt(13)));

  for (var i = 0; i < valida.length; i++) {
    dig1 += i > 0 ? cnpj.charAt(i - 1) * valida[i] : 0;
    dig2 += cnpj.charAt(i) * valida[i];
  }
  dig1 = dig1 % 11 < 2 ? 0 : 11 - (dig1 % 11);
  dig2 = dig2 % 11 < 2 ? 0 : 11 - (dig2 % 11);

  if (dig1 * 10 + dig2 != digito) {
    $('#cnpjErro').prop('hidden', false);
    valido = false;
    HabilitarBotao();
  } else {
    $('#cnpjErro').prop('hidden', true);
    valido = true;
    HabilitarBotao();
  }
}

function HabilitarBotao() {
  var formValido = document.getElementById('formulario').checkValidity();
  if (valido && conectado & formValido && pronto)
    $('#submeter').prop('disabled', false);
  else $('#submeter').prop('disabled', true);
}
