import path from 'path';

// Login
export const LOGIN_PAGE = `https://www3.comprasnet.gov.br/sicaf-web/index.jsf`;
export const GOVERNO_TAB = String.raw`#formLogin\:tipoUsuario > div.ui-button.ui-widget.ui-state-default.ui-button-text-only.ui-corner-right > span`;
export const LOGIN_INPUT = String.raw`#formLogin\:txtCpf`;
export const LOGIN_ID = String.raw`formLogin:txtCpf`; // Usado no findById
export const SENHA_ID = String.raw`formLogin:txtSenha`; // Usado no findById
export const SUBMETER_BUTTON = String.raw`#formLogin\:btnAvancar`;
export const BLOCK_UI_SELECTOR = '.blockOverlay';
export const ERRO_BOX = String.raw`#mensagens\:listaMensagens_container > div > div > div.ui-growl-message > span`;
export const PAGINA_INICIAL = '#titulo';

// Gerar Certidão
export const DOWNLOAD_PATH = path.resolve(__dirname, '..', 'download');
export const URL_DECLARACAO = `https://www3.comprasnet.gov.br/sicaf-web/private/geral/consultarSituacaoFornecedor.jsf`;
export const PESSOA_JURIDICA_SELECTOR = String.raw`#form\:pesq\:tipoPessoaPesquisa > tbody > tr > td:nth-child(2) > label`;
export const CNPJ_SELECTOR = String.raw`#form\:pesq\:cnpjPesquisa`;
export const CNPJ_ID = String.raw`form:pesq:cnpjPesquisa`;
export const PESQUISAR_SELECTOR = String.raw`#form\:pesq\:btnPesquisar`;
export const GERAR_PDF_SELECTOR = String.raw`#form\:fornecedores\:0\:detalharLink`;

// Pesquisar sócios
export const CPF_URL =
  'https://www3.comprasnet.gov.br/sicaf-web/private/consultas/consultaParametrizadaFornecedores.jsf';
export const CNPJ_CPF_SELECTOR = String.raw`#f\:j_idt106\:cnpjPesquisa`;
export const PESQUISAR_CPF_SELECTOR = String.raw`#f\:j_idt106\:btnPesquisar`;
export const SOCIOS_SELECTOR = String.raw`#f\:idListaSocios > legend`;
export const TABELA_DE_SOCIOS_SELECTOR = String.raw`#f\:socios_data`;
export const CNPJ_ID2 = String.raw`f:j_idt106:cnpjPesquisa`;

// Pesquisar dirigentes
export const DIRIGENTE_SELECTOR = String.raw`#f\:idListaDirigentes`;
export const TABELA_DE_DIRIGENTES_SELECTOR = String.raw`#f\:dirigentes_data`;
