export const BASE_URL =
  "https://contas.tcu.gov.br/ords/f?p=1660:3:5437569824565::::P3_TIPO:";
export const SUBMITER_SELECTOR = ".t-Button--hot";
export const DOWNLOAD_SELECTOR =
  "#R2730400877670862202 > div.t-Region-bodyWrap >" +
  " div.t-Region-body > div:nth-child(6) > a";
export const CPF_SELECTOR = "#P3_CPF_INI"
export const CNPJ_SELECTOR = "#P3_CNPJ_INI"
// export const SESSAO_EXPIRADA_BOX = "#ui-id-1 > p"
// export const SESSAO_EXPIRADA_PAGE = "#wwvFlowForm > div > div > div > div > div > div > div.t-Alert-content > div > h3"

