import express from 'express';
import socketIo from 'socket.io';
import http from 'http';
import bodyParser from 'body-parser';
const { fork } = require('child_process');
var path = require('path');

const PORT = 3000;
const app = express();
const server = http.createServer(app);
const io = socketIo(server);

export default function iniciarServidor() {
  console.log(__dirname);
  app.use(express.static(path.resolve(__dirname, 'public')));
  app.use(express.static(path.resolve(__dirname, 'download')));
  app.use(bodyParser.urlencoded({ extended: true }));

  app.get('/', (req, res) => {
    res.sendFile(__dirname + '/public/index.html');
  });

  app.post('/', function(req, res) {
    const forked = fork(path.resolve(__dirname, 'iniciar.js'), [
      req.body.usuario,
      req.body.senha,
      req.body.CNPJ,
      req.body.id
    ]);

    forked.on('message', mensagem => {
      console.log(mensagem);
      emitir(mensagem);
    });
  });

  io.on('connection', function(socket) {
    emitir({ id: socket.id, mensagem: 'conectado' });

    socket.on('disconnect', function() {
      emitir({ id: socket.id, mensagem: 'desconectado' });
    });
  });

  global.emitir = function(mensagem) {
    if (
      mensagem.mensagem == 'iniciado' ||
      mensagem.mensagem == 'terminado' ||
      mensagem.mensagem == 'conectado' ||
      mensagem.mensagem == 'desconectado'
    ) {
      io.to(mensagem.id).emit('notificacao', mensagem.mensagem);
    } else {
      io.to(mensagem.id).emit('mensagem', mensagem.mensagem);
    }
  };

  server.listen(PORT, () => {
    console.log(`Servidor iniciado em http://localhost:${PORT}`);
  });
}

iniciarServidor();
