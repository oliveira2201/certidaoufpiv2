import Logger from './modulos/logger';
import Certidao from './modulos/certidao';

async function iniciar(usuario, senha, CNPJ, id) {
  try {
    console.log(usuario, senha, CNPJ, id);

    global.id = id;
    global.logger = new Logger();
    global.certidao = new Certidao(usuario, senha, CNPJ);
    logger.log('Programa Iniciado');
    logger.log('iniciado');
    await certidao.iniciarNavegador();
    await certidao.logarNoSicaf();
    await certidao.obterPessoasFisicas();
    await certidao.obterPessoaJuridica();
    await certidao.iniciarModulos();
    await certidao.procurarArquivosDoUsuario();
    await certidao.consolidarArquivos();
    await certidao.removerArquivosTemporarios();
    logger.log('Certidões geradas com sucesso');
    await certidao.gerarLinkDaCertidao();
  } catch (erro) {
    console.log(erro);
    logger.log('Não foi possível concluir as operações');
    logger.log(erro);
    await certidao.procurarArquivosDoUsuario();
    await certidao.removerArquivosTemporarios();
  } finally {
    global.browser.close();
    logger.log('Programa terminado');
    logger.log('terminado');
  }
}

iniciar(process.argv[2], process.argv[3], process.argv[4], process.argv[5]);
