import Util from './util';
import * as ceisConst from '../consts/ceis';
import path from 'path';
const PAGE_SELECTOR = '#lista > tbody > tr > td';

export default class Ceis extends Util {
  constructor(cpfCnpj) {
    super(cpfCnpj, 'ceis');
    this.DIRETORIO = path.resolve(__dirname, '..', 'download');
  }

  async iniciar() {
    const URL = ceisConst.BASE_URL + this.CPF_CNPJ;

    const page = await browser.newPage();
    await Promise.all([page.waitForNavigation(), page.goto(URL)]);

    await page.waitForSelector(PAGE_SELECTOR);
    return page
      .pdf({ path: this.DIRETORIO + '/' + this.NOME + '.pdf', format: 'A4' })
      .then(() => {
        logger.log(
          `CEIS: Certidão do CPF/CNPJ ${this.adicionarPontuacao()} gerada`
        );
        return this.NOME + '.pdf';
      });
  }
}
