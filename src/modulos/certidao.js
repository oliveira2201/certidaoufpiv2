// Dependencias
import puppeteer from 'puppeteer';
import pdfMerge from 'easy-pdf-merge';
import fs from 'fs';
import find from 'find';
import path from 'path';
// Modulos
import Sicaf from './sicaf';
import Ceis from './ceis';
import Cnia from './cnia';
import Tcu from './tcu';

export default class Certidao {
  constructor(usuario, senha, cnpj) {
    this.USUARIO = usuario;
    this.SENHA = senha;
    this.CNPJ = cnpj;
    this.LISTA_CPF_CNPJ = [];
    this.SICAF_MODULO = [];
    this.LISTA_DE_ARQUIVOS = [];
    this.DIRETORIO = path.resolve(__dirname, '..', 'download');
    this.NOME = '';
  }

  async iniciarNavegador() {
    global.browser = await puppeteer.launch({
      headless: true,
      // args: ['--no-sandbox', '--disable-setuid-sandbox'],
      ignoreHTTPSErrors: true
    });
  }

  async logarNoSicaf() {
    // Necessário para o módulo iniciarModulos aproveitar a instance da pagina
    this.SICAF_MODULO = new Sicaf(this.USUARIO, this.SENHA, this.CNPJ);
    return await this.SICAF_MODULO.login().catch(e => logger.log(e.message));
  }

  async obterPessoasFisicas() {
    return this.SICAF_MODULO.obterPessoasFisicas().then(lista => {
      this.LISTA_CPF_CNPJ = lista;
    });
  }

  async obterPessoaJuridica() {
    this.LISTA_CPF_CNPJ.push(this.CNPJ);
  }

  async iniciarModulos() {
    const modulos = [];

    // Coloca numa array as Promisses retornadas
    this.LISTA_CPF_CNPJ.forEach(elemento => {
      modulos.push(this.SICAF_MODULO.obterCertidao());
      modulos.push(new Ceis(elemento).iniciar());
      modulos.push(new Tcu(elemento).iniciar());
      modulos.push(new Cnia(elemento).iniciar());
    });

    function allSettled(promises) {
      let wrappedPromises = promises.map(p =>
        Promise.resolve(p).then(
          val => ({ state: 'fulfilled', value: val }),
          err => ({ state: 'rejected', reason: err })
        )
      );
      return Promise.all(wrappedPromises);
    }

    // Espera todas promises resolverem
    return allSettled(modulos).then(values => {
      return values;
    });
  }

  async procurarArquivosDoUsuario() {
    const regex = new RegExp(`.*${id}.*`);
    // Transforma o callback em promessa
    const findPromisse = (_regex, _dir) =>
      new Promise(resolve => find.file(_regex, _dir, resolve));
    return findPromisse(regex, this.DIRETORIO).then(lista => {
      this.LISTA_DE_ARQUIVOS = lista;
    });
  }

  async consolidarArquivos() {
    const CNPJ_SEM_PONTUACAO = this.CNPJ.replace(/[/.-]/g, '');
    // Evitar bug desse arquivo ser coletado na buscaDeArquivosDoUsuario
    this.NOME = `${CNPJ_SEM_PONTUACAO}-${id.substring(2, 17)}.pdf`;
    // Transforma o callback em promessa
    const pdfMergePromise = (_input, _output) =>
      new Promise(resolve => {
        pdfMerge(_input, _output, resolve);
      });
    return pdfMergePromise(
      this.LISTA_DE_ARQUIVOS,
      `${this.DIRETORIO}/${this.NOME}`
    ).then(() => logger.log('Arquivos consolidados'));
  }

  async removerArquivosTemporarios() {
    let arquivosDeletados = [];
    // Coloca numa array as Promisses retornadas
    this.LISTA_DE_ARQUIVOS.forEach(arquivo => {
      arquivosDeletados.push(fs.promises.unlink(arquivo));
    });
    // Espera todas promisses resolverem
    return Promise.all(arquivosDeletados).then(() => {
      logger.log('Arquivos temporários deletados');
    });
  }

  gerarLinkDaCertidao() {
    logger.log(`<a href="/${this.NOME}" target="_blank"> Baixar arquivo </a>'`);
  }
}
