import fs from 'fs';
import path from 'path';
import download from 'download';

export default class Util {
  constructor(cpfCnpj, modulo) {
    this.CPF_CNPJ = this.retirarPontuacao(cpfCnpj);
    this.NOME = this.obterNome(modulo);
    this.DIRETORIO = path.resolve(__dirname, '..', 'download');
  }
  isCpfOuCnpj() {
    if (this.CPF_CNPJ.length === 11) {
      return 'CPF';
    } else {
      return 'CNPJ';
    }
  }

  retirarPontuacao(cpfCnpj) {
    return cpfCnpj.replace(/[/.-]/g, '');
  }

  adicionarPontuacao() {
    const tipo = this.isCpfOuCnpj(this.CPF_CNPJ);
    if (tipo === 'CPF') {
      return this.CPF_CNPJ.replace(
        /^(\d{3})(\d{3})(\d{3})(\d{2})/,
        '$1.$2.$3-$4'
      );
    } else {
      return this.CPF_CNPJ.replace(
        /^(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/,
        '$1.$2.$3/$4-$5'
      );
    }
  }

  async baixarPDF(URL) {
    // const OPTIONS = {
    //   directory: this.DIRETORIO,
    //   filename: this.NOME + '.pdf'
    // };
    // // Transforma o callback em promessa
    // const download = (url, options) =>
    //   new Promise(resolve => downloadPdf(url, options, resolve));
    await download(URL).then(data => {
      fs.writeFileSync(path.resolve(this.DIRETORIO, this.NOME + '.pdf'), data);
    });
  }

  async renomearArquivo(caminho, arquivo) {
    const arquivoAntigo = path.resolve(caminho, arquivo);
    const arquivoNovo = path.resolve(caminho, `${this.NOME}.pdf`);
    let repeticoes = 20;
    const intervalo = 1000;

    let espera = new Promise(async resolve => {
      const repetirFuncao = setInterval(() => {
        const isArquivoExiste = fs.existsSync(arquivoAntigo);
        repeticoes--;
        if (repeticoes === 0 || isArquivoExiste === true) {
          clearInterval(repetirFuncao);
          resolve();
          return fs.promises.rename(arquivoAntigo, arquivoNovo);
        }
      }, intervalo);
    });

    return espera;
  }

  obterNome(modulo) {
    switch (modulo) {
      case 'sicaf':
        return `${id}-(1)sicaf-${this.CPF_CNPJ}`;
      case 'ceis':
        return `${id}-(2)ceis-${this.CPF_CNPJ}`;
      case 'tcu':
        return `${id}-(3)tcu-${this.CPF_CNPJ}`;
      case 'cnia':
        return `${id}-(4)cnia-${this.CPF_CNPJ}`;
    }
  }
}
