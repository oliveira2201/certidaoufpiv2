/* eslint-disable require-jsdoc */
const download = require('download-pdf');
import * as tcuConst from '../consts/tcu';
import Util from './util';

export default class Tcu extends Util {
  constructor(cpfCnpj) {
    super(cpfCnpj, 'tcu');
  }

  async iniciar() {
    const TIPO = this.isCpfOuCnpj();
    const URL = tcuConst.BASE_URL + TIPO;

    // Necessário para não dar sessão expirada
    const context = await browser.createIncognitoBrowserContext();
    const page = await context.newPage();

    const INPUT_SELECTOR =
      TIPO === `CPF` ? tcuConst.CPF_SELECTOR : tcuConst.CNPJ_SELECTOR;

    await page.goto(URL, { waitUntil: 'load' });
    await page.type(INPUT_SELECTOR, this.CPF_CNPJ);
    await page.click(tcuConst.SUBMITER_SELECTOR);
    await page.waitForSelector(tcuConst.DOWNLOAD_SELECTOR);
    const link = await page.evaluate(() => {
      const linkRaw = document.querySelectorAll('a')[8].href;
      return linkRaw;
    });
    return this.baixarPDF(link, this.NOME).then(() => {
      logger.log(
        `TCU: Certidão do CPF/CNPJ ${this.adicionarPontuacao()} gerada`
      );
      return this.NOME + '.pdf';
    });
  }
}
