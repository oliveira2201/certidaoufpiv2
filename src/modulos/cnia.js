import Util from './util';
import * as cniaConst from "../consts/cnia"

/* eslint-disable require-jsdoc */

export default class Cnia extends Util{
  constructor (cpfCnpj) {
    super(cpfCnpj, "cnia");

  }
  // global.logger.enviar('CNIA: Declaração do CPF/CNPJ ' +
  //   transformarCPFCNPJ.adicionarPontuacao(cpfCnpj) + ' gerada');

  async iniciar() {
    const URL = cniaConst.BASE_URL + this.CPF_CNPJ;
    return this.baixarPDF(URL).then(() => {
      logger.log(`CNIA: Certidão do CPF/CNPJ ${this.adicionarPontuacao()} gerada`);
      return this.NOME  + ".pdf";
    });
  }
};

