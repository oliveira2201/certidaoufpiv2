import * as sicafConst from '../consts/sicaf';
import Util from './util';

export default class Sicaf extends Util {
  constructor(usuario, senha, CNPJ) {
    super(CNPJ, 'sicaf');
    this.PAGE = 'asdasd';
    this.USUARIO = usuario;
    this.SENHA = senha;
  }

  async login() {
    this.PAGE = await browser.newPage();
    await this.PAGE.goto(sicafConst.LOGIN_PAGE, {
      waitUntil: 'load'
    });
    await this.PAGE.click(sicafConst.GOVERNO_TAB);
    // Espera a backdrop sumir antes de digitar
    await this.PAGE.waitForSelector(sicafConst.BLOCK_UI_SELECTOR, {
      hidden: true
    });
    await this.PAGE.waitForSelector(sicafConst.LOGIN_INPUT);
    //Injeta diretamente, para evitar problemas com a máscara
    await this.PAGE.evaluate(
      (loginInput, usuario, senhaInput, senha) => {
        (document.getElementById(loginInput).value = usuario),
          (document.getElementById(senhaInput).value = senha);
      },
      sicafConst.LOGIN_ID,
      this.USUARIO,
      sicafConst.SENHA_ID,
      this.SENHA
    );
    await this.PAGE.click(sicafConst.SUBMETER_BUTTON);

    return Promise.race([
      this.PAGE.waitForSelector(sicafConst.ERRO_BOX).then(() => {
        return this.PAGE.evaluate(erroBox => {
          let erro = document.querySelector(erroBox).innerHTML;
          throw `SICAF: ${erro}`;
        }, sicafConst.ERRO_BOX);
      }),
      this.PAGE.waitForSelector(sicafConst.PAGINA_INICIAL).then(() => {
        logger.log('SICAF: Login realizado');
      })
    ]);
  }

  async obterCertidao() {
    await this.PAGE.goto(sicafConst.URL_DECLARACAO, {
      waitUntil: 'load'
    });
    await this.PAGE._client.send('Page.setDownloadBehavior', {
      behavior: 'allow',
      downloadPath: sicafConst.DOWNLOAD_PATH
    });
    await this.PAGE.waitForSelector(sicafConst.CNPJ_SELECTOR);
    await this.PAGE.click(sicafConst.PESSOA_JURIDICA_SELECTOR);
    await this.PAGE.waitForSelector(sicafConst.CNPJ_SELECTOR);
    //Injeta diretamente, para evitar problemas com a máscara
    await this.PAGE.evaluate(
      (CNPJ_ID, CNPJ) => {
        document.getElementById(CNPJ_ID).value = CNPJ;
      },
      sicafConst.CNPJ_ID,
      this.CPF_CNPJ
    );
    await this.PAGE.click(sicafConst.PESQUISAR_SELECTOR);
    // Espera a backdrop sumir antes de digitar
    await this.PAGE.waitForSelector(sicafConst.BLOCK_UI_SELECTOR, {
      hidden: true
    });
    await this.PAGE.waitForSelector(sicafConst.GERAR_PDF_SELECTOR);
    await this.PAGE.click(sicafConst.GERAR_PDF_SELECTOR);
    return this.renomearArquivo(
      sicafConst.DOWNLOAD_PATH,
      'consultarSituacaoFornecedor.pdf'
    ).then(() =>
      logger.log(`SICAF: Certidão do CNPJ ${this.adicionarPontuacao()} gerada`)
    );
  }

  async obterPessoasFisicas() {
    let sociosLista = await this.consultarSocios();
    if (sociosLista) {
      logger.log(`Sócios com maiores participações: ${sociosLista}`);
      return sociosLista;
    } else {
      logger.log('SICAF: Empresa sem sócios. Buscando dirigentes.');
      const dirigentes = await this.consultarDirigentes();
      logger.log(`SICAF: Dirigentes: ${dirigentes}`);
      return dirigentes;
    }
  }

  // ******************************************************//
  // Auxiliares
  // ******************************************************//

  async consultarSocios() {
    await this.PAGE.goto(sicafConst.CPF_URL, {
      waitUntil: 'load'
    });

    //Injeta diretamente, para evitar problemas com a máscara
    await this.PAGE.evaluate(
      (CNPJ_ID, CNPJ) => {
        document.getElementById(CNPJ_ID).value = CNPJ;
      },
      sicafConst.CNPJ_ID2,
      this.CPF_CNPJ
    );
    await this.PAGE.click(sicafConst.PESQUISAR_CPF_SELECTOR);
    await this.PAGE.waitForSelector(sicafConst.SOCIOS_SELECTOR);
    await this.PAGE.click(sicafConst.SOCIOS_SELECTOR);
    // Passar os sócios com com maiores particitações para uma array
    await this.PAGE.waitForSelector(sicafConst.TABELA_DE_SOCIOS_SELECTOR);
    const listaSocios = await this.PAGE.evaluate(() => {
      const listaSocios = [];
      const socioRaw = document
        .querySelector('#f\\:socios_data')
        .querySelectorAll('tr');
      if (socioRaw[0].innerText !== 'Nenhum sócio/administrador retornado') {
        for (let i = 0; i < socioRaw.length; i++) {
          const socio = {
            cpf: socioRaw[i].children[0].innerText,
            participacao: socioRaw[i].children[2].innerText
          };
          listaSocios.push(socio);
        }
      }
      return listaSocios;
    });
    // Caso possui sócios, obter os com maiores participações
    if (listaSocios.length > 0) {
      return this.selecionarSociosComMaioresParticipacoes(listaSocios);
    }
    return null;
  }

  async consultarDirigentes() {
    await this.PAGE.waitForSelector(sicafConst.DIRIGENTE_SELECTOR);
    await this.PAGE.click(sicafConst.DIRIGENTE_SELECTOR);
    await this.PAGE.waitForSelector(sicafConst.TABELA_DE_DIRIGENTES_SELECTOR);
    const listaDirigentes = await this.PAGE.evaluate(() => {
      const listaDirigentes = [];
      const dirigenteRaw = document
        .querySelector('#f\\:dirigentes_data')
        .querySelectorAll('tr');
      for (let i = 0; i < dirigenteRaw.length; i++) {
        const dirigente = dirigenteRaw[i].children[0].innerText;
        listaDirigentes.push(dirigente);
      }
      return listaDirigentes;
    });
    return listaDirigentes;
  }

  selecionarSociosComMaioresParticipacoes(lista) {
    let z = 0;
    let sociosComMaioresParticipacoes = [];
    for (let index = 0; index < lista.length; index++) {
      const socio = lista[index];
      socio.participacao.replace(',', '.');
      socio.participacao = parseFloat(socio.participacao);
      if (socio.participacao > z) {
        z = socio.participacao;
        sociosComMaioresParticipacoes = [socio.cpf];
      } else if (socio.participacao === z) {
        sociosComMaioresParticipacoes.push(socio.cpf);
      }
    }
    return sociosComMaioresParticipacoes;
  }
}
